`timescale 1ns / 1ps

module base4trojan(clk, x, leds);

  input clk;
  wire [15:0] lfsr_out;
  wire enable;
  reg [15:0] key = 16'b0;
  output [15:0] x;
  output [3:0] leds;
  assign x = key;
  
  pulse pulse01 (.clk(clk), .pulse_out(enable));
  
  lfsr lfsr01 (.clk(clk), .lfsr_out(lfsr_out));

  pattern pattern01 (.clk(clk), .rst(1'b0), .key(key),.leds(leds));

  always @(posedge clk) begin
    if (enable) key <= lfsr_out;
  end
  
  
endmodule 
