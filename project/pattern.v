`timescale 1ns / 1ps

module pattern(clk, rst, key, leds);
  localparam state_A = 0, state_B = 1, state_C = 2, state_D = 3;
  
  input clk, rst;
  input [15:0] key;
  output reg [3:0] leds = 4'b0000;
  
  reg [25:0] cnt = 30'b0;
  reg [1:0] state = state_A;
  
  localparam CNTBIT = 24;
  
  wire [2:0] subKey00,subKey01,subKey02,subKey03;
  
  assign subKey00 = key[0] + key[1] + key[2] + key[3];
  assign subKey01 = key[4] + key[5] + key[6] + key[7];
  assign subKey02 = key[8] + key[9] + key[10] + key[11];
  assign subKey03 = key[12] + key[13] + key[14] + key[15];
  
  always @(posedge clk) begin
    if (rst) begin
      state <= state_A;
      leds <= 4'b0100;
      cnt <= 0;
    end else begin
      state <= state;
      cnt <= cnt + 1'b1;
      case (state)
        state_A: begin
          leds <= 4'b0100;
          if (cnt[CNTBIT]==1 && (subKey00 == 4 )) begin
          	state <= state_B;
            cnt <= 0;
          end
        end
        state_B: begin
          leds <= 4'b0010;
          if (cnt[CNTBIT+1]==1 && (subKey01 == 1)) begin
          	state <= state_C;
            cnt <= 0;
          end
        end
		  
        state_C: begin
          leds <= 4'b1000;
          if (cnt[CNTBIT]==1 && (subKey02 == 3)) begin
          	state <= state_D;
            cnt <= 0;
          end
        end
		  
        state_D: begin
          leds <= 4'b0001;
          if (cnt[CNTBIT]==1 && (subKey03 == 2)) begin
          	state <= state_A;
            cnt <= 0;
          end
        end


      endcase
    end
    
  end
  
endmodule
