`timescale 1ns / 1ps


module tb_pulse;

	// Inputs
	reg clk;

	// Outputs
	wire [15:0] x;
	wire [3:0] leds;

	// Instantiate the Unit Under Test (UUT)
	base4trojan uut (
		.clk(clk), 
		.x(x),.leds(leds)
	);

	initial begin
		// Initialize Inputs
		clk = 0;

		// Wait 100 ns for global reset to finish
		#10000;
        
		// Add stimulus here

	end
	
	always #1 clk = ~clk;
      
endmodule

