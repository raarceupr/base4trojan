`timescale 1ns / 1ps




module pulse(clk, pulse_out);
  input clk;
  reg [10:0] ctr = 11'b0;
  reg counted = 1'b0;
  output pulse_out;
  assign pulse_out = ctr == 11'h7e1;
  always @(posedge clk) begin
    if (counted == 1'b0) begin
		 ctr = ctr + 1'b1;
		 if (ctr == 11'h7e2) begin 
			//y <= 1'b1;
			counted = 1'b1;		
		 end
	 end
  end
endmodule

module lfsr(
  input clk,
  output reg [15:0] lfsr_out = 1
);

wire feedback = lfsr_out[15];

always @(posedge clk)
begin
  lfsr_out[0] <= feedback;
  lfsr_out[1] <= lfsr_out[0];
  lfsr_out[2] <= lfsr_out[1] ^ feedback;
  lfsr_out[3] <= lfsr_out[2] ^ feedback;
  lfsr_out[4] <= lfsr_out[3];
  lfsr_out[5] <= lfsr_out[4] ^ feedback;
  lfsr_out[6] <= lfsr_out[5];
  lfsr_out[7] <= lfsr_out[6];
  lfsr_out[8] <= lfsr_out[7];
  lfsr_out[9] <= lfsr_out[8];
  lfsr_out[10] <= lfsr_out[9];
  lfsr_out[11] <= lfsr_out[10];
  lfsr_out[12] <= lfsr_out[11];
  lfsr_out[13] <= lfsr_out[12];
  lfsr_out[14] <= lfsr_out[13];
  lfsr_out[15] <= lfsr_out[14];
end
endmodule
